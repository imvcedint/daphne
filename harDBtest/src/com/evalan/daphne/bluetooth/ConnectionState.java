package com.evalan.daphne.bluetooth;

/** Defines the Bluetooth connection states. */
public enum ConnectionState {
    Connecting, Connected, Disconnecting, Disconnected
}