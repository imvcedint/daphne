package com.evalan.daphne.bluetooth;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;

/** Defines the Bluetooth library callbacks that may be implemented. */
public abstract class BleCallback {
    /** Indicates the given characteristic has been notified/indicated. */
    public void onCharacteristicChanged(BluetoothDevice device, BluetoothGattCharacteristic characteristic) {
    }

    /** Indicates the given characteristic has been read. */
    public void onCharacteristicRead(BluetoothGattCharacteristic characteristic, int status) {
    }

    /** Indicates the given characteristic has been written. */
    public void onCharacteristicWrite(BluetoothGattCharacteristic characteristic, int status) {
    }

    /** Indicates the connection state has changed. */
    public void onConnectionStateChange(int status, ConnectionState state) {
    }

    /** Indicates the given descriptor has been read. */
    public void onDescriptorRead(BluetoothGattDescriptor descriptor, int status) {
    }

    /** Indicates the given descriptor has been written. */
    public void onDescriptorWrite(BluetoothGattDescriptor descriptor, int status) {
    }

    /** Indicates the signal strength has been retrieved. */
    public void onReadRemoteRssi(int rssi, int status) {
    }

    /** Indicates the reliable write operation has finished. */
    public void onReliableWriteCompleted(int status) {
    }

    /** Indicates the GATT service discovery process has finished. */
    public void onServicesDiscovered(int status) {
    }
}