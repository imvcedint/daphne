package com.evalan.daphne.bluetooth;

import java.util.HashMap;
import java.util.UUID;

/** Contains the known Bluetooth GATT characteristics, descriptors and services. */
public class Known {
    /** Known GATT services */
    public static final HashMap<UUID, String> SERVICES = getServices();
    /** Known GATT characteristics */
    public static final HashMap<UUID, String> CHARACTERISTICS = getCharacteristics();
    /** Known GATT descriptors */
    public static final HashMap<UUID, String> DESCRIPTORS = getDescriptors();

    /** Adds known characteristics to static hashmap. */
    private static HashMap<UUID, String> getCharacteristics() {
        HashMap<UUID, String> map = new HashMap<>();

        // Bluetooth standard GATT characteristics.
        map.put(UUID.fromString("00002A00-0000-1000-8000-00805f9b34fb"), "Device Name");
        map.put(UUID.fromString("00002A01-0000-1000-8000-00805f9b34fb"), "Appearance");
        map.put(UUID.fromString("00002A02-0000-1000-8000-00805f9b34fb"), "Peripheral Privacy Flag");
        map.put(UUID.fromString("00002A03-0000-1000-8000-00805f9b34fb"), "Reconnection Address");
        map.put(UUID.fromString("00002A04-0000-1000-8000-00805f9b34fb"), "Preferred Connection Parameters");
        map.put(UUID.fromString("00002A05-0000-1000-8000-00805f9b34fb"), "Service Changed");
        map.put(UUID.fromString("00002A19-0000-1000-8000-00805f9b34fb"), "Battery Level");
        map.put(UUID.fromString("00002A24-0000-1000-8000-00805f9b34fb"), "Model Number String");
        map.put(UUID.fromString("00002A26-0000-1000-8000-00805f9b34fb"), "Firmware Revision String");
        map.put(UUID.fromString("00002A27-0000-1000-8000-00805f9b34fb"), "Hardware Revision String");
        map.put(UUID.fromString("00002A29-0000-1000-8000-00805f9b34fb"), "Manufacturer Name String");

        // Daphne accelerometer GATT characteristics.
        map.put(UUID.fromString("36c9AAA1-2486-420f-b40c-bddb99af7c29"), "Accelerometer Data");
       map.put(UUID.fromString("36c9AAA2-2486-420f-b40c-bddb99af7c29"), "Accelerometer Enabler");
        map.put(UUID.fromString("36c9AAA3-2486-420f-b40c-bddb99af7c29"), "Accelerometer Sample Rate");

        // Daphne gyroscope GATT characteristics.
        map.put(UUID.fromString("36c9ABA1-2486-420f-b40c-bddb99af7c29"), "Gyroscope Data");
        map.put(UUID.fromString("36c9ABA2-2486-420f-b40c-bddb99af7c29"), "Gyroscope Enabler");
        map.put(UUID.fromString("36c9ABA3-2486-420f-b40c-bddb99af7c29"), "Gyroscope Sample Rate");


    //    Daphne time GATT characteristics.
        map.put(UUID.fromString("36c9AFA1-2486-420f-b40c-bddb99af7c29"), "UNIX Timestamp");
        map.put(UUID.fromString("36c9AFA2-2486-420f-b40c-bddb99af7c29"), "UNIX Timestamp (Start)");

        return map;
    }

    /** Adds known descriptors to static hashmap. */
    private static HashMap<UUID, String> getDescriptors() {
        HashMap<UUID, String> map = new HashMap<>();
        // Bluetooth standard GATT descriptors.
        map.put(UUID.fromString("00002900-0000-1000-8000-00805f9b34fb"), "Extended Properties");
        map.put(UUID.fromString("00002901-0000-1000-8000-00805f9b34fb"), "User Description");
        map.put(UUID.fromString("00002902-0000-1000-8000-00805f9b34fb"), "Client Configuration");
        map.put(UUID.fromString("00002903-0000-1000-8000-00805f9b34fb"), "Server Configuration");
        map.put(UUID.fromString("00002904-0000-1000-8000-00805f9b34fb"), "Presentation Format");
        map.put(UUID.fromString("00002905-0000-1000-8000-00805f9b34fb"), "Aggregate Format");
        map.put(UUID.fromString("00002906-0000-1000-8000-00805f9b34fb"), "Valid Range");
        return map;
    }

    /** Adds known services to static hashmap. */
    private static HashMap<UUID, String> getServices() {
        HashMap<UUID, String> map = new HashMap<>();

        // Bluetooth standard GATT services.
        map.put(UUID.fromString("00001800-0000-1000-8000-00805f9b34fb"), "Generic Access");
        map.put(UUID.fromString("00001801-0000-1000-8000-00805f9b34fb"), "Generic Attribute");
        map.put(UUID.fromString("00001805-0000-1000-8000-00805f9b34fb"), "Current Time Service");
        map.put(UUID.fromString("00001806-0000-1000-8000-00805f9b34fb"), "Reference Time Update Service");
        map.put(UUID.fromString("0000180A-0000-1000-8000-00805f9b34fb"), "Device Information");
        map.put(UUID.fromString("0000180F-0000-1000-8000-00805f9b34fb"), "Battery Service");

        return map;
    }
}