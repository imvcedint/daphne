package com.evalan.daphne.bluetooth;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.content.Context;

import com.evalan.daphne.util.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/** Represents a Bluetooth low energy (BLE) connection with a single peripheral. */
public class BleConnection {
	
	
	
	//Modifications by UPM for the experiments of Human Activity Recognition 
	
	private  short harActivity;
	private String directory;
	 public short getHarActivity()
	    {
	    	return harActivity;
	    }
	    
	    public void setHarActivity( short harActivity )
	    {
	    	this.harActivity = harActivity;
	    }
	    public String getDirectory()
	    {
	    	return directory;
	    }
	    
	    public void setDirectory( String directory )
	    {
	    	this.directory = directory;
	    }
	
    /** UUID for notify descriptor */
    private static final UUID DESCRIPTOR_CHARACTERISTIC_CONFIGURATION = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");
    /** BleManager instance */
    private final BleManager bleManager;
    /** Local BLE callback implementation */
    private final Callback localCallback;
    /** Bluetooth device associated with connection */
    private final BluetoothDevice device;
    /** Registered BLE callback handlers */
    private final List<BleCallback> externalCallbacks;
    /** Bluetooth GATT object associated with connection */
    private BluetoothGatt bluetoothGatt;
    /** Current connection state */
    private ConnectionState connectionState;

    /** Instantiates a managed Bluetooth low energy connection. */
    public BleConnection(BluetoothDevice device) {
        this.bleManager = BleManager.getInstance();
        this.localCallback = new Callback();
        this.device = device;
        this.externalCallbacks = new ArrayList<>();
        this.bluetoothGatt = null;
        this.connectionState = ConnectionState.Disconnected;
        // Register this connection with the manager.
        bleManager.register(this);
    }

    /** Obtains the Bluetooth device object associated with this connection. */
    public final BluetoothDevice getDevice() {
        return device;
    }

    /** Adds the Bluetooth callback handler to receive status callbacks. */
    public final void addCallback(BleCallback callback) {
        externalCallbacks.add(callback);
    }

    /** Establishes the BLE connection. */
    public final void connect(final Context context) {
        bleManager.enqueue(new Runnable() {
            @Override
            public void run() {
                if (bluetoothGatt == null) {
                    connectionState = ConnectionState.Connecting;
                    bluetoothGatt = device.connectGatt(context, false, localCallback);
                } else {
                    bleManager.dequeue();
                }
            }
        });
    }

    /** Terminates the BLE connection. */
    public final void disconnect() {
        if (bluetoothGatt != null) {
            connectionState = ConnectionState.Disconnected;
            bluetoothGatt.close();
            bluetoothGatt = null;
        }
    }

    /** Indicates whether the connection is considered to be active. */
    public final boolean isConnected() {
        return connectionState == ConnectionState.Connected;
    }

    /** Aborts a reliable write operation. */
    public final void abortReliableWrite() {
        throw new UnsupportedOperationException("Method abortReliableWrite() is not available in API level 18");
        // if (bluetoothGatt != null)
        // bluetoothGatt.abortReliableWrite();
    }

    /** Starts a reliable write operation. */
    public final void beginReliableWrite() {
        bleManager.enqueue(new Runnable() {
            @Override
            public void run() {
                if (bluetoothGatt == null || !bluetoothGatt.beginReliableWrite()) {
                    Logger.d("beginReliableWrite() not scheduled");
                    bleManager.dequeue();
                }
            }
        });
    }

    /** Discovers all GATT services offered by the connected peripheral. */
    public final void discoverServices() {
        bleManager.enqueue(new Runnable() {
            @Override
            public void run() {
                if (bluetoothGatt == null || !bluetoothGatt.discoverServices()) {
                    Logger.d("discoverServices() not scheduled");
                    bleManager.dequeue();
                }
            }
        });
    }

    /** Executes a reliable write operation. */
    public final void executeReliableWrite() {
        bleManager.enqueue(new Runnable() {
            @Override
            public void run() {
                if (bluetoothGatt == null || !bluetoothGatt.executeReliableWrite()) {
                    Logger.d("executeReliableWrite() not scheduled");
                    bleManager.dequeue();
                }
            }
        });
    }

    /** Gets a reference to the GATT service identified by the given UUID. */
    public final BluetoothGattService getService(UUID uuid) {
        return (bluetoothGatt != null) ? bluetoothGatt.getService(uuid) : null;
    }

    /** Gets a list of all GATT services offered by the connected peripheral. */
    public final List<BluetoothGattService> getServices() {
        return (bluetoothGatt != null) ? bluetoothGatt.getServices() : new ArrayList<BluetoothGattService>();
    }

    /** Reads the given GATT characteristic. */
    public final void readCharacteristic(final BluetoothGattCharacteristic characteristic) {
        bleManager.enqueue(new Runnable() {
            @Override
            public void run() {
                if (bluetoothGatt == null || !bluetoothGatt.readCharacteristic(characteristic)) {
                    Logger.d("readCharacteristic() not scheduled");
                    bleManager.dequeue();
                }
            }
        });
    }

    /** Reads the given GATT descriptor. */
    public final void readDescriptor(final BluetoothGattDescriptor descriptor) {
        bleManager.enqueue(new Runnable() {
            @Override
            public void run() {
                if (bluetoothGatt == null || !bluetoothGatt.readDescriptor(descriptor)) {
                    Logger.d("readDescriptor() not scheduled");
                    bleManager.dequeue();
                }
            }
        });
    }

    /** Reads the remote signal strength. */
    public final void readRemoteRssi() {
        bleManager.enqueue(new Runnable() {
            @Override
            public void run() {
                if (bluetoothGatt == null || !bluetoothGatt.readRemoteRssi()) {
                    Logger.d("readRemoteRssi() not scheduled");
                    bleManager.dequeue();
                }
            }
        });
    }

    /** Requests a new connection interval/timeout/latency. */
    public final boolean requestConnectionPriority(int connectionPriority) {
        throw new UnsupportedOperationException("Method requestConnectionPriority() is not available in API level 18");
        // return bluetoothGatt.requestConnectionPriority(connectionPriority);
    }

    /** Request a MTU size. */
    public final boolean requestMtu(int mtu) {
        throw new UnsupportedOperationException("Method requestMtu() is not available in API level 18");
        // return bluetoothGatt.requestMtu(mtu);
    }

    /** Enables notification for the given GATT characteristic. */
    public final void setCharacteristicNotification(final BluetoothGattCharacteristic characteristic, final boolean enable) {
        final BluetoothGattDescriptor descriptor = characteristic.getDescriptor(DESCRIPTOR_CHARACTERISTIC_CONFIGURATION);

        if (descriptor != null) {
            // Enable local notification callback.
            if (bluetoothGatt != null)
                bluetoothGatt.setCharacteristicNotification(characteristic, enable);

            // Enable notify for characteristic.
            bleManager.enqueue(new Runnable() {
                @Override
                public void run() {
                    descriptor.setValue(enable ? BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE : BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE);
                    if (bluetoothGatt == null || !bluetoothGatt.writeDescriptor(descriptor)) {
                        Logger.d("setCharacteristicNotification() not scheduled");
                        bleManager.dequeue();
                    }
                }
            });
        }
    }

    /** Writes the given GATT characteristic. */
    public final void writeCharacteristic(final BluetoothGattCharacteristic characteristic) {
        bleManager.enqueue(new Runnable() {
            @Override
            public void run() {
                if (bluetoothGatt == null || !bluetoothGatt.writeCharacteristic(characteristic)) {
                    Logger.d("writeCharacteristic() not scheduled");
                    bleManager.dequeue();
                }
            }
        });
    }

    /** Writes the given GATT descriptor. */
    public final void writeDescriptor(final BluetoothGattDescriptor descriptor) {
        bleManager.enqueue(new Runnable() {
            @Override
            public void run() {
                if (bluetoothGatt == null || !bluetoothGatt.writeDescriptor(descriptor)) {
                    Logger.d("writeDescriptor() not scheduled");
                    bleManager.dequeue();
                }
            }
        });
    }

    /** Local callback object, handles dequeue operations to prevent BLE stack from crashing. */
    private final class Callback extends BluetoothGattCallback {
        @Override
        public final void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            Logger.v("onCharacteristicChanged()");

            // Notify callback handlers.
            for (BleCallback callback : externalCallbacks)
                callback.onCharacteristicChanged(gatt.getDevice(), characteristic);
        }

        @Override
        public final void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            Logger.d("onCharacteristicRead()", printableStatus(status), Arrays.toString(characteristic.getValue()));

            // Close BLE connection upon error.
            if (status != BluetoothGatt.GATT_SUCCESS && status != BluetoothGatt.GATT_INSUFFICIENT_AUTHENTICATION)
                disconnect();

            // Notify callback handlers.
            for (BleCallback callback : externalCallbacks)
                callback.onCharacteristicRead(characteristic, status);

            // Run next item in queue.
            bleManager.dequeue();
        }

        @Override
        public final void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            Logger.d("onCharacteristicWrite()", printableStatus(status), Arrays.toString(characteristic.getValue()));

            // Close BLE connection upon error.
            if (status != BluetoothGatt.GATT_SUCCESS)
                disconnect();

            // Notify callback handlers.
            for (BleCallback callback : externalCallbacks)
                callback.onCharacteristicWrite(characteristic, status);

            // Run next item in queue.
            bleManager.dequeue();
        }

        @Override
        public final void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            // Close BLE connection upon error.
            if (status != BluetoothGatt.GATT_SUCCESS)
                disconnect();

            ConnectionState state = connectionState;
            if (status == BluetoothGatt.GATT_SUCCESS) {
                switch (newState) {
                    case BluetoothGatt.STATE_CONNECTING:
                        state = ConnectionState.Connecting;
                        break;
                    case BluetoothGatt.STATE_CONNECTED:
                        state = ConnectionState.Connected;
                        break;
                    case BluetoothGatt.STATE_DISCONNECTING:
                        state = ConnectionState.Disconnecting;
                        break;
                    case BluetoothGatt.STATE_DISCONNECTED:
                        state = ConnectionState.Disconnected;
                        break;
                }
            }

            connectionState = state;

            // Notify callback handlers.
            for (BleCallback callback : externalCallbacks)
                callback.onConnectionStateChange(status, state);

            Logger.d("onConnectionStateChange()", printableStatus(status), state.name());

            // Run next item in queue.
            bleManager.dequeue();
        }

        @Override
        public final void onDescriptorRead(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            Logger.d("onDescriptorRead()", printableStatus(status), Arrays.toString(descriptor.getValue()));

            // Close BLE connection upon error.
            if (status != BluetoothGatt.GATT_SUCCESS)
                disconnect();

            // Notify callback handlers.
            for (BleCallback callback : externalCallbacks)
                callback.onDescriptorRead(descriptor, status);

            // Run next item in queue.
            bleManager.dequeue();
        }

        @Override
        public final void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            Logger.d("onDescriptorWrite()", printableStatus(status));

            // Close BLE connection upon error.
            if (status != BluetoothGatt.GATT_SUCCESS)
                disconnect();

            // Notify callback handlers.
            for (BleCallback callback : externalCallbacks)
                callback.onDescriptorWrite(descriptor, status);

            // Run next item in queue.
            bleManager.dequeue();
        }

        @Override
        public final void onReadRemoteRssi(BluetoothGatt gatt, int rssi, int status) {
            Logger.d("onReadRemoteRssi()", printableStatus(status), rssi);

            // Close BLE connection upon error.
            if (status != BluetoothGatt.GATT_SUCCESS)
                disconnect();

            // Notify callback handlers.
            for (BleCallback callback : externalCallbacks)
                callback.onReadRemoteRssi(rssi, status);

            // Run next item in queue.
            bleManager.dequeue();
        }

        @Override
        public final void onReliableWriteCompleted(BluetoothGatt gatt, int status) {
            Logger.d("onReliableWriteCompleted()", printableStatus(status));

            // Close BLE connection upon error.
            if (status != BluetoothGatt.GATT_SUCCESS)
                disconnect();

            // Notify callback handlers.
            for (BleCallback callback : externalCallbacks)
                callback.onReliableWriteCompleted(status);

            // Run next item in queue.
            bleManager.dequeue();
        }

        @Override
        public final void onServicesDiscovered(BluetoothGatt gatt, int status) {
            Logger.d("onServicesDiscovered()", printableStatus(status));

            // Close BLE connection upon error.
            if (status != BluetoothGatt.GATT_SUCCESS)
                disconnect();

            // Notify callback handlers.
            for (BleCallback callback : externalCallbacks)
                callback.onServicesDiscovered(status);

            // Run next item in queue.
            bleManager.dequeue();
        }
    }

    /** Translates the given status code to a printable status string. */
    private String printableStatus(int status) {
        switch (status) {
            case BluetoothGatt.GATT_SUCCESS:
                return "status: success (" + status + ")";
            case BluetoothGatt.GATT_CONNECTION_CONGESTED:
                return "status: connection congested (" + status + ")";
            case BluetoothGatt.GATT_FAILURE:
                return "status: failure (" + status + ")";
            case BluetoothGatt.GATT_INSUFFICIENT_AUTHENTICATION:
                return "status: insufficient authentication (" + status + ")";
            case BluetoothGatt.GATT_INSUFFICIENT_ENCRYPTION:
                return "status: insufficient encryption (" + status + ")";
            case BluetoothGatt.GATT_INVALID_ATTRIBUTE_LENGTH:
                return "status: invalid attribute length (" + status + ")";
            case BluetoothGatt.GATT_INVALID_OFFSET:
                return "status: invalid offset (" + status + ")";
            case BluetoothGatt.GATT_READ_NOT_PERMITTED:
                return "status: read not permitted (" + status + ")";
            case BluetoothGatt.GATT_REQUEST_NOT_SUPPORTED:
                return "status: request not supported (" + status + ")";
            case BluetoothGatt.GATT_WRITE_NOT_PERMITTED:
                return "status: write not permitted (" + status + ")";
            case 19:
                return "status: bonding cancelled (" + status + ")";
            case 22:
                return "status: bonding rejected (" + status + ")";
            default:
                return "status: unknown (" + status + ")";
        }
    }
}