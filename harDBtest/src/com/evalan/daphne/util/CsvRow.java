package com.evalan.daphne.util;

/** Interface contract for CSV exportable domain entities. */
public interface CsvRow {
    /** Converts this domain entity to a .CSV row. */
    String toCsvRow();
}