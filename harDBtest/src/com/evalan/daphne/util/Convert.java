package com.evalan.daphne.util;

import java.nio.ByteBuffer;

/** Provides a set of static methods to convert hexadecimal strings to byte array and vice versa. */
public class Convert {
    /** Converts the given hexadecimal string to byte array. */
    public static byte[] hexString(String in) {
        byte[] out = new byte[0];

        if (in != null && in.length() != 0) {
            String[] parts = in.split(" ");
            ByteBuffer bb = ByteBuffer.allocate(parts.length);
            for (String part : parts) {
                bb.put(Byte.parseByte(part, 16));
            }
            out = bb.array();
        }

        return out;
    }

    /** Converts the given byte array to hexadecimal string. */
    public static String hexString(byte[] in) {
        String out = new String();

        if (in != null && in.length != 0) {
            StringBuilder sb = new StringBuilder();
            for (byte b : in) {
                if (sb.length() != 0)
                    sb.append(" ");
                sb.append(Integer.toHexString(b & 0xFF));
            }
            out = sb.toString();
        }

        return out;
    }
}