package com.evalan.daphne.util;

import android.util.Log;

/** Provides a set of static methods to write log messages. */
public class Logger {
    private static final String LOG_TAG = "Daphne";
    private static final int LOG_LEVEL = Log.DEBUG;

    /** Send a {@link android.util.Log#DEBUG} log message. */
    public static void d(Object... args) {
        if (isLogAllowed(Log.DEBUG))
            Log.d(LOG_TAG, log(args));
    }

    /** Send a {@link android.util.Log#DEBUG} log message. */
    public static void d(Throwable t) {
        if (isLogAllowed(Log.DEBUG))
            Log.d(LOG_TAG, log(t));
    }

    /** Send a {@link android.util.Log#ERROR} log message. */
    public static void e(Object... args) {
        if (isLogAllowed(Log.ERROR))
            Log.e(LOG_TAG, log(args));
    }

    /** Send a {@link android.util.Log#ERROR} log message. */
    public static void e(Throwable t) {
        if (isLogAllowed(Log.ERROR))
            Log.e(LOG_TAG, log(t));
    }

    /** Send a {@link android.util.Log#INFO} log message. */
    public static void i(Object... args) {
        if (isLogAllowed(Log.INFO))
            Log.i(LOG_TAG, log(args));
    }

    /** Send a {@link android.util.Log#INFO} log message. */
    public static void i(Throwable t) {
        if (isLogAllowed(Log.INFO))
            Log.i(LOG_TAG, log(t));
    }

    /** Indicates whether log level is loggable. */
    private static boolean isLogAllowed(int level) {
        return level >= LOG_LEVEL;
    }

    /** Logs the given objects (toString()). */
    private static String log(Object... args) {
        if (args == null)
            return "NULL";

        StringBuilder builder = new StringBuilder();
        for (Object arg : args) {
            if (arg == null) {
                builder.append("NULL");
            } else {
                builder.append(arg);
            }
            builder.append(" ");
        }

        return builder.toString();
    }

    /** Logs the given throwable. */
    private static String log(Throwable t) {
        if (t == null)
            return "NULL";

        StackTraceElement[] elements = t.getStackTrace();

        StringBuilder builder = new StringBuilder();
        if (t.getMessage() != null)
            builder.append(t.getMessage());
        builder.append("\n");
        for (StackTraceElement element : elements) {
            builder.append(element);
            builder.append("\n");
        }

        return builder.toString();
    }

    /** Send a {@link android.util.Log#VERBOSE} log message. */
    public static void v(Object... args) {
        if (isLogAllowed(Log.VERBOSE))
            Log.v(LOG_TAG, log(args));
    }

    /** Send a {@link android.util.Log#VERBOSE} log message. */
    public static void v(Throwable t) {
        if (isLogAllowed(Log.VERBOSE))
            Log.v(LOG_TAG, log(t));
    }

    /** Send a {@link android.util.Log#WARN} log message. */
    public static void w(Object... args) {
        if (isLogAllowed(Log.WARN))
            Log.w(LOG_TAG, log(args));
    }

    /** Send a {@link android.util.Log#WARN} log message. */
    public static void w(Throwable t) {
        if (isLogAllowed(Log.WARN))
            Log.w(LOG_TAG, log(t));
    }
}