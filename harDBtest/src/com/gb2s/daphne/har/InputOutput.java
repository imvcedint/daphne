package com.gb2s.daphne.har;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import android.os.Environment;

public class InputOutput
{
	private static String path = "DaphneGB2S";
	private static String extensionTXT = ".txt";
	
	public static void initFiles()
	{
		File file = new File(  Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + path );
		if( !file.exists() ) file.mkdir();
	}
	
	public static String newSession( User user ) throws IOException
	{
		String pathComplete = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + path;
		File fComplete = new File( pathComplete );
		if( !fComplete.exists() )
		{
			fComplete.mkdir();
		}
		File fUsers = new File( pathComplete + "/" + user.getId() );
		if( !fUsers.exists() )  
		{
			fUsers.mkdir();
			fUsers = new File( pathComplete + "/" + user.getId() + "/" + "user_" + String.valueOf( user.getId() ) + extensionTXT );
			BufferedWriter out = new BufferedWriter( new FileWriter( fUsers ) );
			out.write( userInfoToCSV( user ) );
			out.flush();
			out.close();
		}	
			long timestamp  = System.currentTimeMillis();
			String fSession = user.getId() + "/" + String.valueOf( timestamp ) + "/";
			String fSessionComplete = pathComplete + "/" + fSession;
			File fSessionFile = new File( fSessionComplete );
			if( !fSessionFile.exists() )
			{
				fSessionFile.mkdir();
			}
			
			return path + "/" + fSession;		
	}
	
	public void saveAntropData( User user, String path ) throws IOException
	{
		File file = new File (path);
	    BufferedWriter out = new BufferedWriter(new FileWriter(file)); 
		out.write( userInfoToCSV( user ) );
		out.close();
	}
	
	private static String userInfoToCSV( User user )
	{
		String userData = "";
		userData = userData + String.valueOf( user.getId()  )       + "," ;
		userData = userData + String.valueOf( user.getAge() )       + "," ;
		userData = userData + String.valueOf( user.getHeight() )    + "," ;
		userData = userData + String.valueOf( user.getWeight() )    + ","  ;
		userData = userData + String.valueOf( user.getGender()   );
		userData = userData + String.valueOf( user.getDominantArm() + ",") ;
		return userData;
	}
	
	public static void readMe() throws IOException 
	{
		String pathReadme = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + path + "/" + "README" + extensionTXT;
		File   fReadme = new File( pathReadme );
		if( !fReadme.exists() )
		{
			BufferedWriter out = new BufferedWriter(new FileWriter(fReadme)); 
			out.write("############################ README ############################ ");
			out.write("User data is saved in the following format: UserID, Age, Height[cm], Weight[Kg], "
				+ "Gender[0 = Male, 1 = Female], Dominant Arm[0 = Right, 1 = Left ]");
			out.flush();
			out.close();
		}
	}
	
	public static void storeData( ArrayList<Double> data, String path ) throws IOException
	{
		File fPath = new File( path );
		BufferedWriter out = new BufferedWriter( new FileWriter( path ) ) ;
		for( int i = 0; i < data.size() - 1; i++ )
		{
			out.write( String.valueOf( data.get(i) ) + "," );
		}
		out.write( String.valueOf( data.get(data.size()-1) ) );
		out.write("\n" );
		out.flush();
		out.close();
	}
}















