package com.gb2s.daphne.har;

import java.util.UUID;

import com.evalan.daphne.bluetooth.BleCallback;
import com.evalan.daphne.bluetooth.BleConnection;
import com.evalan.daphne.bluetooth.BleManager;
import com.evalan.daphne.bluetooth.DaphneBleConnection;
import com.evalan.daphne.bluetooth.Known;
import com.evalan.daphne.util.Convert;
import com.gb2s.daphne.har.user_interface.ListAdapter;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class ActivitiesActivity extends FragmentActivity
{	
	private static String loggerTag = "DAPHNE_DB";
	private static final UUID DESCRIPTOR_USER_DESCRIPTION = UUID.fromString("00002901-0000-1000-8000-00805f9b34fb");    
    private BleCallback callback;
    private ListAdapter<BluetoothGattCharacteristic> adapter;
    private BleConnection connectionSensor60;
    private BleConnection connectionSensor57;
    
    private BluetoothAdapter bluetooth;
    private ListView list;
    private Button connect;
    private boolean connected = false;
    
    private int numSensors = 0;    
	private boolean recording;
	private Chronometer chrono;
	private long timeInit;
	private long timeEnd;
	
	ListView listActivities;
	
	private Button buttonConnect;
    private Button buttonSession;
    private Button buttonExit;
    
    private short currentActivity;
    
    
	@Override
	public void onCreate( Bundle savedInstance )
	{
		super.onCreate( savedInstance );
		recording = false;
		setContentView( R.layout.har_layout );
		chrono = (Chronometer) findViewById( R.id.chrono_activities  );
		timeInit = 0;
		timeEnd  = 0;
		currentActivity = -1;
		
		String pathSession = "";
		Bundle extras = getIntent().getExtras();
		if( extras != null )
		{
			pathSession = extras.getString( "pathSession" );
			TextView textPathSession = (TextView) findViewById(R.id.textPahtSession);
			textPathSession.setText( pathSession );
		}
		
	    listActivities = (ListView)findViewById(android.R.id.list);
	    listActivities.setOnItemClickListener( new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
			//	Log.i(loggerTag,"###Has pinchado el elemento: " + String.valueOf( position ) + ", con valor = " + listActivities.getItemAtPosition( position ));
				currentActivity = (short) position;
				if( connectionSensor57 != null )
				{
					   connectionSensor57.setHarActivity( currentActivity );
				}
				if( connectionSensor60 != null )
				{
					connectionSensor60.setHarActivity( currentActivity );
				}
			}
	    	
		}  );
		
		bluetooth = BluetoothAdapter.getDefaultAdapter();
        BluetoothDevice deviceSensor57 = bluetooth.getRemoteDevice("00:07:80:7E:F4:57");     
        BluetoothDevice deviceSensor60 = bluetooth.getRemoteDevice("00:07:80:7F:06:60");
        connectionSensor57 = new DaphneBleConnection( this.getApplicationContext(), deviceSensor57 );
        connectionSensor60 = new DaphneBleConnection( this.getApplicationContext(), deviceSensor60 );
        connectionSensor57.setDirectory( pathSession );
        connectionSensor60.setDirectory( pathSession );
        connectionSensor57.setHarActivity( currentActivity );
        connectionSensor60.setHarActivity( currentActivity );
        
        buttonConnect = (Button) findViewById( R.id.buttonConnect );
        buttonSession = (Button) findViewById( R.id.buttonNewSession );       
        
        buttonExit = (Button) findViewById( R.id.buttonExit );
        buttonExit.setOnClickListener( new View.OnClickListener() 
        {
        	public void onClick( View v )
        	{
        		((DaphneBleConnection) connectionSensor57).disconnect();
        		((DaphneBleConnection) connectionSensor60).disconnect();
        		((DaphneBleConnection) connectionSensor57).clear();
        		((DaphneBleConnection) connectionSensor60).clear();
        		finish();
        	}
        });
        
        
        buttonSession.setOnClickListener( new View.OnClickListener()
        {
        	 public void onClick( View v )
        	 {
        		 ((DaphneBleConnection) connectionSensor57).setUnixTimestamp();
        		 ((DaphneBleConnection) connectionSensor60).setUnixTimestamp();
        	 }
         });
         
         buttonConnect.setOnClickListener(new View.OnClickListener() {
             public void onClick(View v) {
             	if(connected)
             	{                     
             		if( connectionSensor57.isConnected() )
             		{
             			connectionSensor57.disconnect();
             			numSensors--;
             			((DaphneBleConnection) connectionSensor57).enableAccelerometer(false);
                        ((DaphneBleConnection) connectionSensor57).enableGyroscope(false);
                        ((DaphneBleConnection) connectionSensor57).clear();	
             		}
             	
             		if( connectionSensor60.isConnected() )
             		{
             			connectionSensor60.disconnect();
             			
             			numSensors--;
             			((DaphneBleConnection) connectionSensor60).enableAccelerometer(false);
                        ((DaphneBleConnection) connectionSensor60).enableGyroscope(false);
                        ((DaphneBleConnection) connectionSensor60).clear();	
             		}
             		
                	if( !connectionSensor57.isConnected() && !connectionSensor60.isConnected() )
             		{
             			connected = false;
             		}
             		buttonConnect.setText("Connect");
             	}
             	else
             	{
             		if( connectionSensor57 != null )
             		{
             			 if( this != null && connectionSensor57.isConnected() )
             		    	{
             		    		numSensors++;
             		    		Toast.makeText( getApplicationContext(), "Connected Sensors: " + String.valueOf( numSensors ) ,Toast.LENGTH_SHORT).show();
             		    		Toast.makeText( getApplicationContext(), "Connected with Sensor #57", Toast.LENGTH_SHORT).show();
            		    	}
             			 
             			((DaphneBleConnection) connectionSensor57).setUnixTimestamp();
                 		((DaphneBleConnection) connectionSensor57).enableAccelerometer(true);
                 		((DaphneBleConnection) connectionSensor57).enableGyroscope(true);
                 		((DaphneBleConnection) connectionSensor57).getStartTimestamp();
                 		
                 		connectionSensor57.addCallback( callback );
                 		
                 		if( connectionSensor57.isConnected() && connectionSensor60.isConnected() )
                 		{
                 			connected = true;
                 			buttonConnect.setText("Disconnect");
                 		}
             		}
             		
             		
             		
             		if( connectionSensor60 != null )
             		{
                 			 if( this != null && connectionSensor60.isConnected() )
                 		    	{
                 		    		numSensors++;
                 		    		Toast.makeText( getApplicationContext(), "Connected Sensors: " + String.valueOf( numSensors ) ,Toast.LENGTH_SHORT).show();
                 		    		Toast.makeText( getApplicationContext(), "Connected with Sensor #60", Toast.LENGTH_SHORT).show();
                		    	} 
                 			 
             			((DaphneBleConnection) connectionSensor60).setUnixTimestamp();
                 		((DaphneBleConnection) connectionSensor60).enableAccelerometer(true);
                 		((DaphneBleConnection) connectionSensor60).enableGyroscope(true);
                 		((DaphneBleConnection) connectionSensor60).getStartTimestamp();	
                 		
                 		connectionSensor60.addCallback(callback);
                 		
                 		if( connectionSensor57.isConnected() && connectionSensor60.isConnected() )
                 		{
                 			connected = true;
                 			buttonConnect.setText("Disconnect");
                 		}	
             		}
             	}
             }
         });
         
         if( this != null && !connectionSensor57.isConnected() )
         {
         	connectionSensor57.connect( this.getApplicationContext() );
         }
         
         if (this != null && !connectionSensor60.isConnected())
         {
         	connectionSensor60.connect(this.getApplicationContext());        	
         	
         }
         
         // BLE callback definition.
         callback = new BleCallback() {
             @Override
             public void onCharacteristicChanged(final BluetoothDevice device, final BluetoothGattCharacteristic characteristic) {
                 if (this != null) {                	
                     // Update user interface.
                     	runOnUiThread(new Runnable() {
                         @Override
                         public void run() {
                             adapter.notifyDataSetChanged();
                         }
                     });
                 }
             }
             
             @Override
             public void onCharacteristicRead(BluetoothGattCharacteristic characteristic, int status) {
                 if (status == BluetoothGatt.GATT_SUCCESS && this != null) {
                     // Update user interface.
                 	
                     	runOnUiThread(new Runnable() {
                         @Override
                         public void run() {
                             adapter.notifyDataSetChanged();
                         }
                     });
                 }
             }

             @Override
             public void onDescriptorRead(BluetoothGattDescriptor descriptor, int status) {
                 if (status == BluetoothGatt.GATT_SUCCESS && this != null) {
                     // Update user interface.

                     	runOnUiThread(new Runnable() {
                         @Override
                         public void run() {
                             adapter.notifyDataSetChanged();
                         }
                     });
                 }
             }
         };

         adapter = new ListAdapter<BluetoothGattCharacteristic>(this, R.layout.characteristic_list_item) {
             @Override
             protected View getView(View view, BluetoothGattCharacteristic item) {
                             	
             	if( item.getUuid().toString().toLowerCase().equals("36c9AAA1-2486-420f-b40c-bddb99af7c29".toLowerCase()) 
             			|| item.getUuid().toString().toLowerCase().equals("36c9ABA1-2486-420f-b40c-bddb99af7c29".toLowerCase()))
             	{
             	 ((TextView) view.findViewById(R.id.text1)).setText(item.getUuid().toString().substring(4, 8));
                	 ((TextView) view.findViewById(R.id.text2)).setText(Known.CHARACTERISTICS.get(item.getUuid()));
                	 ((TextView) view.findViewById(R.id.text3)).setText(Convert.hexString(item.getValue()));
             	}            	
                 return view;
             }
         };
	}
	
	public void record( View v)
	{
		chrono 			  = (Chronometer) findViewById( R.id.chrono_activities );	
		if( recording == false )
		{
			recording = true;
			chrono.start();
			timeInit = System.currentTimeMillis();
		}
		else
		{
			recording = false;
			chrono.stop();
			timeEnd = System.currentTimeMillis();
		}
	}	
	
	@Override
	public void onDestroy()
	{
		super.onDestroy();
		BleManager.getInstance().clear();
		finish();
	}
}








