package com.gb2s.daphne.har.user_interface;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;

/** Generic list adapter with a backing <code>ArrayList</code>. */
public abstract class ListAdapter<T> extends BaseAdapter {
    /** List */
    private final ArrayList<T> list = new ArrayList<>();
    /** Application context */
    private final Context context;
    /** XML resource identifier */
    private final int resource;

    /**
     * <p>Default constructor.</p>
     *
     * @param context  Application context
     * @param resource Identifier for XML resource to load (e.g. R.layout.main_page)
     */
    public ListAdapter(Context context, int resource) {
        this.context = context;
        this.resource = resource;
    }

    /**
     * <p>Adds the specified object at the end of the backing <code>ArrayList</code>.</p>
     */
    public void add(T item) {
        if (!list.contains(item))
            list.add(item);
    }

    /**
     * <p>Removes all elements from the backing <code>ArrayList</code>, leaving it empty. Invalidates the data set.</p>
     *
     * @see #notifyDataSetInvalidated()
     */
    public void clear() {
        notifyDataSetInvalidated();
        list.clear();
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public T getItem(int position) {
        return list.get(position);
    }

    /**
     * <p>Obtains all objects from the backing <code>ArrayList</code>.</p>
     */
    public ArrayList<T> getItems() {
        return list;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(resource, parent, false);
        }

        return getView(convertView, getItem(position));
    }

    /**
     * <p>Implementations should modify and return the passed view.</p>
     */
    protected abstract View getView(View view, T item);
}