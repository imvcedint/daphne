package com.gb2s.daphne.har;


import java.io.IOException;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends FragmentActivity	 {

	private static String loggerTag = "DAPHNE_DB";
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		InputOutput.initFiles();
		
		Button harButton = (Button) findViewById( R.id.button_har );
		harButton.setOnClickListener( new View.OnClickListener() {
			@Override
			public void onClick(View v) {
			
				TextView textId      =        (TextView)findViewById( R.id.field_id     );
				TextView textAge     =        (TextView)findViewById( R.id.field_age    );
				TextView textHeight  =        (TextView)findViewById( R.id.field_height );
				TextView textWeight  =        (TextView)findViewById( R.id.field_weight );
				Spinner  spinnerGender =      (Spinner)findViewById(  R.id.spinner_gender );
				Spinner  spinnerDominantArm = (Spinner)findViewById( R.id.spinner_dominant_arm );
	
				if( validateTextView( textId ) == false || validateTextView( textAge ) == false  
						|| validateTextView( textHeight ) == false || validateTextView( textWeight ) == false )
				{
					Toast.makeText( getApplicationContext(), "Error in the input data", Toast.LENGTH_SHORT ).show();
				}
				else
				{
				 	User user = new User( Integer.valueOf( textId.getText().toString() ),  spinnerGender.getSelectedItemPosition(),
							Double.valueOf( textAge.getText().toString() ), Double.valueOf( textHeight.getText().toString()),
							Double.valueOf( textWeight.getText().toString() ), spinnerDominantArm.getSelectedItemPosition() );
					String pathSession;
					try {
						pathSession = InputOutput.newSession( user  );
						Log.d(loggerTag, "Ficheros inicializados. Path: " + pathSession );
						Intent intent = new Intent( MainActivity.this, ActivitiesActivity.class );
						startActivity( intent );
					} catch (IOException e) {
						Log.e(loggerTag, "Error al guardar los datos");
						Toast.makeText( getApplicationContext(), "Error la guardar los datos", Toast.LENGTH_SHORT ).show();
						e.printStackTrace();
					}
					
				}
			}
		});		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) 
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		int id = item.getItemId();
		if (id == R.id.action_settings)
		{
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private boolean validateTextView( TextView textView )
	{
		if( textView.getText().toString().trim().length() > 0 )
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	
}
