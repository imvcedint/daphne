package com.gb2s.daphne.har;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.Toast;

public class ActivitiesActivity extends FragmentActivity
{
	private Button buttonStartStop;
	private final String textStart = "Start";
	private final String textStop  = "Stop";
	private boolean recording;
	private Chronometer chrono;
	private long timeInit;
	private long timeEnd;
	
	@Override
	public void onCreate( Bundle savedInstance )
	{
		super.onCreate( savedInstance );
		recording = false;
		setContentView( R.layout.har_layout );
		buttonStartStop = (Button)findViewById( R.id.button_startstop );
		buttonStartStop.setText( textStart );
		chrono = (Chronometer) findViewById( R.id.chrono_activities  );
		timeInit = 0;
		timeEnd  = 0;
	}
	
	public void record( View v)
	{
		buttonStartStop   = (Button)findViewById( R.id.button_startstop );
		chrono 			  = (Chronometer) findViewById( R.id.chrono_activities );
		if( recording == false )
		{
			buttonStartStop.setText( textStart );			
			Toast.makeText( v.getContext(), "Empezando captura", Toast.LENGTH_SHORT).show();
			recording = true;
			chrono.start();
			timeInit = System.currentTimeMillis();
		}
		else
		{
			buttonStartStop.setText( textStop );
			Toast.makeText( v.getContext(), "Parando captura", Toast.LENGTH_SHORT).show();
			recording = false;
			chrono.stop();
			timeEnd = System.currentTimeMillis();
		}
	}	
}








