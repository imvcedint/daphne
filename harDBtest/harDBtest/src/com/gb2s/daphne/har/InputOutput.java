package com.gb2s.daphne.har;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import android.os.Environment;

public class InputOutput
{
	
	private static String path = "DaphneGB2S";
	private static String extensionTXT = ".txt";
	private static String extensionCSV = ".csv";

	
	public static void initFiles()
	{
		File file = new File(  Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + path );
		if( !file.exists() ) file.mkdir();
	}
	
	public static String newSession( User user ) throws IOException
	{
		
		String pathComplete = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + path;
		File fUsers = new File( pathComplete + "/" + user.getId() );
		if( !fUsers.exists() )  
		{
			fUsers.mkdir();
			fUsers = new File( pathComplete + "/" + user.getId() + "/" + "user_" + String.valueOf( user.getId() ) + extensionTXT );
			BufferedWriter out = new BufferedWriter( new FileWriter( fUsers ) );
			out.write( String.valueOf( user.getId() ) + ","  );
		}	
			long timestamp  = System.currentTimeMillis();
			String fSession = pathComplete + "/" + user.getId() + "/" + String.valueOf( timestamp ) + extensionTXT ;
			return fSession;		
	}
	
	public void saveAntropData( User user, String path ) throws IOException
	{
		File file = new File (path);
	    BufferedWriter out = new BufferedWriter(new FileWriter(file)); 
		out.write( userInfoToCSV( user ) );
		out.close();
	}
	
	private String userInfoToCSV( User user )
	{
		String userData = "";
		userData = userData + String.valueOf( user.getId()  )       + "," ;
		userData = userData + String.valueOf( user.getAge() )       + "," ;
		userData = userData + String.valueOf( user.getHeight() )    + "," ;
		userData = userData + String.valueOf( user.getWeight() )    + ","  ;
		userData = userData + String.valueOf( user.getDominantArm() + ",") ;
		userData = userData + String.valueOf( user.getGender()   );		
		return userData;
	}
}








