package com.evalan.daphne.domain;

import com.evalan.daphne.util.CsvRow;

/** Represents a single accelerometer measurement. */
public class Accelerometer implements CsvRow {
    private final int timestamp;
    private final short activityID;
    private final short x;
    private final short y;
    private final short z;

    public Accelerometer(int timestamp, short activityID, short x, short y, short z) {
        this.timestamp = timestamp;
        this.activityID = activityID;
        this.x = x;
        this.y = y;
        this.z = z;
    }

    @Override
    public String toCsvRow() {
        return Integer.toString(timestamp) + ";" + Short.toString( activityID ) + ";" + Short.toString(x) + ";" + Short.toString(y) + ";" + Short.toString(z);
    }
}