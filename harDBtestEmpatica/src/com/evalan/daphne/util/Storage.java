package com.evalan.daphne.util;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

/** Provides a set of static methods to write to local storage. */
public class Storage {
    /** Indicates whether external storage is writable. */
    private static boolean isWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }

    /** Obtain a reference to a file in the given directory with the given file name. */
    private static File getFile(Context context, String directoryName, String fileName) throws IOException {
        File sdCard = Environment.getExternalStorageDirectory();
        File directory = new File(sdCard, directoryName);
        // Create directory if it doesn't exist.
        if (!directory.exists())
            directory.mkdir();
        File file = new File(directory, fileName);
        // Create file if it doesn't exist.
       if (!file.exists())
       {
    	   file.createNewFile();
       }


        // Broadcast the intent to run the media scanner to make it visible in the file explorer.
        context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(file)));
        return file;
    }

    /** Writes the given domain entities to the given file name in the given directory. */
    public static void writeCsvFile(Context context, String directoryName, String fileName, List<? extends CsvRow> rows) {
        if (isWritable()) {
            BufferedWriter writer = null;
            try {
                // Obtain reference to file.
                File file = getFile(context, directoryName, fileName);
                // Obtain a writer object.
                writer = new BufferedWriter(new FileWriter(file, true));
                // Convert domain entities to CSV rows.
                for (CsvRow row : rows) {
                    writer.write(row.toCsvRow());
                    writer.newLine();
                    // Flush the writer object.
                    writer.flush();
                }
                Logger.d("Logging", directoryName, fileName, file.length());
            } catch (IOException e) {
                Logger.e(e);
            } finally {
                if (writer != null) {
                    try {
                        writer.close();
                    } catch (IOException e) {
                        Logger.e(e);
                    }
                }
            }
        }
    }
}