package com.evalan.daphne.bluetooth;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.Context;

import com.evalan.daphne.domain.Accelerometer;
import com.evalan.daphne.domain.Gyroscope;
import com.evalan.daphne.util.Storage;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class DaphneBleConnection extends BleConnection {

	
    /** Daphne sensor services */
    public static final UUID SERVICE_ACCELEROMETER = UUID.fromString("36c9AAAA-2486-420f-b40c-bddb99af7c29");
    public static final UUID SERVICE_GYROSCOPE = UUID.fromString("36c9ABAA-2486-420f-b40c-bddb99af7c29");
    public static final UUID SERVICE_CUSTOM_TIME = UUID.fromString("36c9AFAA-2486-420f-b40c-bddb99af7c29");

    /** Daphne accelerometer sensor characteristics */
    public static final UUID CHARACTERISTIC_ACCELEROMETER_DATA = UUID.fromString("36c9AAA1-2486-420f-b40c-bddb99af7c29"),
            CHARACTERISTIC_ACCELEROMETER_ENABLER = UUID.fromString("36c9AAA2-2486-420f-b40c-bddb99af7c29"),
            CHARACTERISTIC_ACCELEROMETER_SAMPLE_RATE = UUID.fromString("36c9AAA3-2486-420f-b40c-bddb99af7c29");
    /** Daphne gyroscope sensor characteristics */
    public static final UUID CHARACTERISTIC_GYROSCOPE_DATA = UUID.fromString("36c9ABA1-2486-420f-b40c-bddb99af7c29"),
            CHARACTERISTIC_GYROSCOPE_ENABLER = UUID.fromString("36c9ABA2-2486-420f-b40c-bddb99af7c29"),
            CHARACTERISTIC_GYROSCOPE_SAMPLE_RATE = UUID.fromString("36c9ABA3-2486-420f-b40c-bddb99af7c29");
    /** Daphne time-related characteristics */
    public static final UUID CHARACTERISTIC_UNIX_TIMESTAMP = UUID.fromString("36c9AFA1-2486-420f-b40c-bddb99af7c29"),
            CHARACTERISTIC_UNIX_TIMESTAMP_START = UUID.fromString("36c9AFA2-2486-420f-b40c-bddb99af7c29");

    /** Characteristic enable/disable values */
    private static final byte[] VALUE_DISABLE_CHARACTERISTIC = new byte[]{0x00}, VALUE_ENABLE_CHARACTERISTIC = new byte[]{0x01};
    /** Storage directory for log files */
    //Modifications by GB2S for the Human Activity Recognition experiments
    private String directory = "";
    
    //private static final String DIRECTORY = "Daphne";
    /** Amount of measurement values to buffer before adding them to the .CSV file */
    private static final int COUNT_ACCELEROMETER = 10, COUNT_GYROSCOPE = 10;
    private final List<Accelerometer> accelerometerData = new ArrayList<>(COUNT_ACCELEROMETER);
    private final List<Gyroscope> gyroscopeData = new ArrayList<>(COUNT_GYROSCOPE);
    private int timestamp;
    
  
    
    public void clear() 
    {
        accelerometerData.clear();
        gyroscopeData.clear();
    }
    
    // Sets the UNIX timestamp.
    public void setUnixTimestamp() {
        // Obtain custom time service handle.
        BluetoothGattService timeService = getService(SERVICE_CUSTOM_TIME);
        if (timeService != null) {
            // Write current timestamp to UNIX timestamp characteristic.
            BluetoothGattCharacteristic timeCharacteristic = timeService.getCharacteristic(CHARACTERISTIC_UNIX_TIMESTAMP);
            if (timeCharacteristic != null) {
                ByteBuffer buffer = ByteBuffer.allocate(4);
                buffer.putInt((int) (System.currentTimeMillis() / 1000));
                timeCharacteristic.setValue(buffer.array());
                writeCharacteristic(timeCharacteristic);
            }
        }
    }

    /** Gets the measurement start UNIX timestamp. */
    public void getStartTimestamp() {
        // Obtain custom time service handle.
        BluetoothGattService timeService = getService(SERVICE_CUSTOM_TIME);
        if (timeService != null) {
            BluetoothGattCharacteristic startTimeCharacteristic = timeService.getCharacteristic(CHARACTERISTIC_UNIX_TIMESTAMP_START);
            if (startTimeCharacteristic != null) {
                readCharacteristic(startTimeCharacteristic);
            }
        }
    }

    /** Sets the enabled state for the given service / characteristic. */
    private void enableCharacteristic(UUID serviceUuid, UUID enablerCharacteristicUuid, UUID characteristicUuid, boolean enable) {
        // Obtain service handle.
        BluetoothGattService service = getService(serviceUuid);
        if (service != null) {
            // Set enabler.
            BluetoothGattCharacteristic enabler = service.getCharacteristic(enablerCharacteristicUuid);
            if (enabler != null) {
                enabler.setValue(enable ? VALUE_ENABLE_CHARACTERISTIC : VALUE_DISABLE_CHARACTERISTIC);
                writeCharacteristic(enabler);
            }

            // Set notify on data characteristic.
            BluetoothGattCharacteristic characteristic = service.getCharacteristic(characteristicUuid);
            if (characteristic != null) {
                setCharacteristicNotification(characteristic, enable);
            }
        }
    }

    /** Sets the accelerometer sensor enabled state. */
    public void enableAccelerometer(boolean enable) {
        enableCharacteristic(SERVICE_ACCELEROMETER, CHARACTERISTIC_ACCELEROMETER_ENABLER, CHARACTERISTIC_ACCELEROMETER_DATA, enable);
    }

    /** Sets the gyroscope sensor enabled state. */
    public void enableGyroscope(boolean enable) {
        enableCharacteristic(SERVICE_GYROSCOPE, CHARACTERISTIC_GYROSCOPE_ENABLER, CHARACTERISTIC_GYROSCOPE_DATA, enable);
    }

    /** Instantiates a managed Bluetooth low energy connection. */
    public DaphneBleConnection(final Context context, final BluetoothDevice device) {
        super(device);

        addCallback(new BleCallback() {
            @Override
            public void onCharacteristicChanged(BluetoothDevice device, BluetoothGattCharacteristic characteristic) {
            	//Modificaciones hechas por GB2S para los experimentos de Reconocimiento de Actividad (HAR)
                // Parse accelerometer data.
                if (characteristic.getUuid().equals(CHARACTERISTIC_ACCELEROMETER_DATA)) {
                    ByteBuffer buffer = ByteBuffer.wrap(characteristic.getValue());
                    buffer.order(ByteOrder.BIG_ENDIAN);
                    accelerometerData.add(new Accelerometer(buffer.getInt(0), getHarActivity(), buffer.getShort(4), buffer.getShort(6), buffer.getShort(8)));
                    accelerometerData.add(new Accelerometer(buffer.getInt(10), getHarActivity(), buffer.getShort(14), buffer.getShort(16), buffer.getShort(18)));
                    if (accelerometerData.size() == COUNT_ACCELEROMETER) {
                    	//Modifications by UPM for the experiments of Human Activity Recognition
                        Storage.writeCsvFile(context, getDirectory(), device.getAddress() + "." + timestamp + ".accelerometer.csv", accelerometerData);
                        accelerometerData.clear();
                    }
                }
                //Modificaciones hechas por GB2S para los experimentos de Reconocimiento de Actividad (HAR)
                // Parse gyroscope data.
                if (characteristic.getUuid().equals(CHARACTERISTIC_GYROSCOPE_DATA)) {
                    ByteBuffer buffer = ByteBuffer.wrap(characteristic.getValue());
                    buffer.order(ByteOrder.BIG_ENDIAN);
                    gyroscopeData.add(new Gyroscope(buffer.getInt(0),getHarActivity(), buffer.getShort(4), buffer.getShort(6), buffer.getShort(8)));
                    gyroscopeData.add(new Gyroscope(buffer.getInt(10), getHarActivity(), buffer.getShort(14), buffer.getShort(16), buffer.getShort(18)));
                    if (gyroscopeData.size() == COUNT_GYROSCOPE) {
                    	//Modifications by UPM for the experiments of Human Activity Recognition
                        Storage.writeCsvFile(context, getDirectory(), device.getAddress() + "." + timestamp + ".gyroscope.csv", gyroscopeData);
                        gyroscopeData.clear();
                    }
                }
            }
            
            @Override
            public void onCharacteristicRead(BluetoothGattCharacteristic characteristic, int status) {
                if (status == BluetoothGatt.GATT_SUCCESS) {
                    // Parse start timestamp data.
                    if (characteristic.getUuid().equals(CHARACTERISTIC_UNIX_TIMESTAMP_START)) {
                        ByteBuffer buffer = ByteBuffer.wrap(characteristic.getValue());
                        if (buffer.getInt(0) > 0) {
                            timestamp = buffer.getInt(0);
                        } else {
                            readCharacteristic(characteristic);
                        }
                    }
                }
            }

            @Override
            public void onConnectionStateChange(int status, ConnectionState state) {
                // Discover services on successful BLE connection.
                if (status == BluetoothGatt.GATT_SUCCESS && state == ConnectionState.Connected) {
                    discoverServices();
                }
            }
        });
    }
}