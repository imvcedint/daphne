package com.evalan.daphne.bluetooth;

import android.bluetooth.BluetoothDevice;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;

/** Manages Bluetooth low energy (BLE) connections. */
public class BleManager {
    /** Singleton instance reference */
    private static BleManager instance;
    /** Map used to keep references to BLE connections */
    private final HashMap<BluetoothDevice, BleConnection> connections;
    /** BLE operation queue */
    private final LinkedList<Runnable> queue;
    /** Busy status indicator */
    private boolean busy;

    /** Instantiates the BLE manager. */
    private BleManager() {
        this.connections = new HashMap<>();
        this.queue = new LinkedList<>();
        this.busy = false;
    }

    /** Obtain a reference to the singleton BLE manager. */
    public synchronized static BleManager getInstance() {
        if (instance == null)
            instance = new BleManager();

        return instance;
    }

    /** Closes all managed BLE connections. */
    public synchronized void close() {
        for (BleConnection connection : connections.values()) {
            connection.disconnect();
        }
    }

    /** Gets a list of all managed BLE connections. */
    public synchronized Collection<BleConnection> get() {
        return connections.values();
    }

    /** Indicates whether a BLE connection with the given device is managed. */
    public synchronized boolean contains(BluetoothDevice device) {
        return connections.containsKey(device);
    }

    /** Gets a reference to the BLE connection with the given device. */
    public synchronized BleConnection get(BluetoothDevice device) {
        return connections.get(device);
    }

    /** Closes all managed BLE connections and clears the list. */
    public synchronized void clear() {
        close();

        connections.clear();
    }

    /** Closes the BLE connection with the given device. */
    public synchronized void remove(BluetoothDevice device) {
        if (connections.containsKey(device))
            connections.get(device).disconnect();

        connections.remove(device);
    }

    /** Indicates a GATT operation finished, runs next operation in queue. */
    synchronized void dequeue() {
        if (!queue.isEmpty()) {
            queue.poll().run();
        } else {
            busy = false;
        }
    }

    /** Adds a new GATT operation to the queue. */
    synchronized void enqueue(Runnable task) {
        queue.add(task);

        if (!queue.isEmpty() && !busy) {
            busy = true;
            queue.poll().run();
        }
    }

    /** Registers the given BLE connection with the manager. */
    synchronized void register(BleConnection connection) {
        connections.put(connection.getDevice(), connection);
    }
}