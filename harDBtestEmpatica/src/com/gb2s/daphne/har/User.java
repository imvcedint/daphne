package com.gb2s.daphne.har;

public class User
{
	
  private int id;
  private int session;
  private int gender;
  private double age;
  private double height;
  private double weight;
  private int dominantArm;
  private int sweatingLevel;
  
  public User( int id, int session, int gender, double age, double height, double weight, int dominantArm, int sweatingLevel )
  {
	  this.id            = id;
	  this.session       = session;
	  this.gender        = gender;
	  this.age           = age;
	  this.height        = height;
	  this.weight        = weight;
	  this.dominantArm   = dominantArm;
	  this.sweatingLevel = sweatingLevel;			  	  
  }
  public User( int id, int gender, double age, double height, double weight, int dominantArm )
  {
	  this.id          = id;
	  this.gender      = gender;
	  this.age         = age;
	  this.height      = height;
	  this.weight      = weight;
	  this.dominantArm = dominantArm;
	  
  }
  public User()
  {
	  
  }
  public int getId()
  {
	  return id;
  }
  public int getSession()
  {
	  return session;
  }
  public int getGender()
  {
	  return gender;
  }
  public double getAge()
  {
	  return age;
  }
  public double getHeight()
  {
	  return height;
  }
  public double getWeight()
  {
	  return weight;
  }
  public int getDominantArm()
  {
	  return dominantArm;
  }
  public int getSweatingLevel()
  {
	  return sweatingLevel;
  }
  public void setId( int id )
  {
	  this.id = id;
  }
  public void setSession( int session )
  {
	  this.session = session;
  }
  public void setGender( int gender )
  {
	  this.gender = gender;
  }
  public void setAge( double age )
  {
	  this.age = age;
  }
  public void setHeight( double height )
  {
	  this.height = height;
  }
  public void setWeight( double weight )
  {
	  this.weight = weight;
  }
  public void setDominantArm( int dominantArm )
  {
	  this.dominantArm = dominantArm;
  }
  public void setSweatingLevel( int sweatingLevel )
  {
	  this.sweatingLevel = sweatingLevel;
  }
}










