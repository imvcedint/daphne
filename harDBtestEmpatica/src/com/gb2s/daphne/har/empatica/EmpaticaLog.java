package com.gb2s.daphne.har.empatica;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;

import android.content.Context;

public class EmpaticaLog {
	
	private double acc_sample[];
	private double bvp_sample[];
	private double ibi_sample[];
	private double gsr_sample[];
	private double tem_sample[];
	private double bat_sample[];
	private double ind_sample[];
	private double last_time=-1;
	PrintWriter accFile = null;
	PrintWriter bvpFile = null;
	PrintWriter ibiFile = null;
	PrintWriter gsrFile = null;
	PrintWriter temFile = null;
	PrintWriter batFile = null;
	PrintWriter indFile = null;
	private String filename;
	
	public EmpaticaLog(String f)
	{
		filename=f;
		acc_sample=new double[4];
		bvp_sample=new double[2];
		ibi_sample=new double[2];
		gsr_sample=new double[2];
		tem_sample=new double[2];
		bat_sample=new double[2];
		ind_sample=new double[2];		
				
		try 
		{
			accFile = new PrintWriter(new FileOutputStream(new File(filename+".acc")));
			bvpFile = new PrintWriter(new FileOutputStream(new File(filename+".bvp")));
			ibiFile = new PrintWriter(new FileOutputStream(new File(filename+".ibi")));
			gsrFile = new PrintWriter(new FileOutputStream(new File(filename+".gsr")));
			temFile = new PrintWriter(new FileOutputStream(new File(filename+".tem")));
			batFile = new PrintWriter(new FileOutputStream(new File(filename+".bat")));
			indFile = new PrintWriter(new FileOutputStream(new File(filename+".ind")));			
		} 
		catch (FileNotFoundException e) 
		{
			// TODO Auto-generated catch block
		    e.printStackTrace();
		}
		
		
		
	}
	
	
	public void UpdateAcc(double timestamp,int x,int y, int z)
	{
		acc_sample[0]=timestamp;
		acc_sample[1]=x;
		acc_sample[2]=y;
		acc_sample[3]=z;		
		SaveSample(accFile,acc_sample);
		last_time=timestamp;
	}
	
	public void UpdateBVP(double timestamp,float bvp)
	{
		bvp_sample[0]=timestamp;
		bvp_sample[1]=bvp;
		SaveSample(bvpFile,bvp_sample);
		last_time=timestamp;				
	}
	
	public void UpdateIBI(double timestamp,float ibi)
	{
		ibi_sample[0]=timestamp;
		ibi_sample[1]=ibi;				
		SaveSample(ibiFile,ibi_sample);
		last_time=timestamp;
	}
	
	public void UpdateGSR(double timestamp,float gsr)
	{
		gsr_sample[0]=timestamp;
		gsr_sample[1]=gsr;
		SaveSample(gsrFile,gsr_sample);
		last_time=timestamp;
	}
	
	public void UpdateTemp(double timestamp,float temp)
	{
		tem_sample[0]=timestamp;
		tem_sample[1]=temp;
		SaveSample(temFile,tem_sample);
		last_time=timestamp;
	}
	
	public void UpdateBattery(double timestamp,float battery)
	{
		bat_sample[0]=timestamp;
		bat_sample[1]=battery;
		SaveSample(batFile,bat_sample);
		last_time=timestamp;
	}
	
	public void UpdateLabel(int l)	
	{
		
	 ind_sample[0]=last_time;
	 ind_sample[1]=l;
	 SaveSample(indFile,ind_sample);	 		
		
	}
	
				
	public void ClearData()
	{
		for(int i=0;i<4;i++)
			acc_sample[i]=0;
		for(int i=0;i<2;i++)
		{
			bvp_sample[i]=0;
			ibi_sample[i]=0;
			gsr_sample[i]=0;
			tem_sample[i]=0;
			bat_sample[i]=0;
			ind_sample[i]=0;			
		}
	}
	
	public void SaveSample(PrintWriter file,double sample[])
	{						
		for (int j = 0; j < sample.length; j++) 
		{
			file.print(String.valueOf(sample[j]));
			file.print(" ");					
		}
				
		file.print("\n");
		file.flush();
	}
	
	public String getFilename()
	{
		return(filename);
	}
	
	public void CloseLog()
	{
		accFile.close();
		bvpFile.close();
		ibiFile.close();
		gsrFile.close();
		temFile.close();
		batFile.close();
		indFile.close();		
	}


				

	
}


