package com.gb2s.daphne.har;

import android.app.ListFragment;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Chronometer;
import android.widget.ListView;
import android.widget.Toast;

public class ActivitiesFragment extends ListFragment
{

	private Chronometer chrono;
	private long timeWhenStopped = 0;
	private long timeLapse = 0;
	
	@Override
    public void onListItemClick(ListView l, View v, int position, long id) 
    {
    	super.onListItemClick(l, v, position, id);
    
    	
    }
  
	@Override
	public View onCreateView( LayoutInflater inflater,
			ViewGroup container, Bundle savedInstance )
			{
			    View v = inflater.inflate( R.layout.har_fragment_layout, container, false );
			    chrono = (Chronometer)v.findViewById(R.id.chrono_activities);
			    chrono.setBase(SystemClock.elapsedRealtime());
			   // listActivities = (ListView)v.findViewById(android.R.id.list);
				return v;
			}	
	
	@Override
    public void onCreate(Bundle savedInstanceState) 
	{
        super.onCreate(savedInstanceState);
    }
	
	
	private void resetChrono( View v )
	{
		chrono.stop();
		timeLapse =  SystemClock.elapsedRealtime() - chrono.getBase();
		//Toast.makeText( getActivity().getApplicationContext(), "Tiempo: " + timeLapse, Toast.LENGTH_SHORT).show();
		chrono.setBase( SystemClock.elapsedRealtime() );		
		timeWhenStopped = 0;
	}
}




