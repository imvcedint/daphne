package com.gb2s.daphne.har;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import com.empatica.empalink.ConnectionNotAllowedException;
import com.empatica.empalink.EmpaDeviceManager;
import com.empatica.empalink.config.EmpaSensorStatus;
import com.empatica.empalink.config.EmpaSensorType;
import com.empatica.empalink.config.EmpaStatus;
import com.empatica.empalink.delegate.EmpaDataDelegate;
import com.empatica.empalink.delegate.EmpaStatusDelegate;
import com.evalan.daphne.bluetooth.BleCallback;
import com.evalan.daphne.bluetooth.BleConnection;
import com.evalan.daphne.bluetooth.BleManager;
import com.evalan.daphne.bluetooth.DaphneBleConnection;
import com.evalan.daphne.bluetooth.Known;
import com.evalan.daphne.util.Convert;
import com.gb2s.daphne.har.empatica.EmpaticaLog;
import com.gb2s.daphne.har.user_interface.ListAdapter;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.ContactsContract.CommonDataKinds.Email;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class ActivitiesActivity extends FragmentActivity implements EmpaDataDelegate, EmpaStatusDelegate
{	
	
	
	private static String loggerTag = "DAPHNE_DB";
    private BleCallback callback;
    private ListAdapter<BluetoothGattCharacteristic> adapter;
    private BleConnection connectionSensor60;
    private BleConnection connectionSensor57;
    
    private BluetoothAdapter bluetooth;
    private ListView list;
    private Button connect;
    private boolean connected = false;
    
    private int numSensors = 0;    
	private boolean recording;
	private Chronometer chrono;
	
	private long timeInit;
	private long timeEnd;
	private long timeLapse = 0;
	private long timeWhenStopped = 0;
	
	private ListView listActivities;
	
	private Button buttonConnect;
    private Button buttonSession;
    private Button buttonExit;
    
    private short currentActivity;
 
	
	private int EMPATICA_SCANNING=0;
	private int EMPATICA_CONNECTED=1;
	private int EMPATICA_STREAMING=2;	
	private int EMPATICA_DISCONNECTED=3;
	private int empatica_state;
	private EmpaticaLog empaticaLog;	
	private BluetoothDevice empatica;
	private EmpaDeviceManager deviceManager;
	private static final int REQUEST_ENABLE_BT = 1;
	private static final long STREAMING_TIME = 100000;
	
	private Button buttonEmpatica;
	private String root_dir = "/sdcard/DaphneGB2S/";
    private String pathSession;

	@Override
	public void onCreate( Bundle savedInstance )
	{
		super.onCreate( savedInstance );
		setContentView( R.layout.har_layout );
		deviceManager = new EmpaDeviceManager(getApplicationContext(), this, this);
		// Register the device manager using your API key. You need to have Internet access at this point.
			deviceManager.authenticateWithAPIKey("32089a4cf3df4ac3ae1a125ca3819a2a"); // TODO insert your API Key here

		buttonEmpatica = (Button)findViewById(R.id.buttonEmpatica);
		buttonEmpatica.setEnabled( false );
		
	
		pathSession = "";
		Bundle extras = getIntent().getExtras();
		if( extras != null )
		{
			pathSession = extras.getString( "pathSession" );
			TextView textPathSession = (TextView) findViewById(R.id.textPahtSession);
			textPathSession.setText( pathSession );
		}
		
		
		buttonEmpatica.setOnClickListener(new OnClickListener(){
            public void onClick(View arg0) {
            	if(empatica_state==EMPATICA_CONNECTED)
            	{
            		try {
            			SimpleDateFormat formatter = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
            			Date now = new Date();            		
            			//filename=root_dir+formatter.format(now);
            			empaticaLog = new EmpaticaLog("/sdcard/" + pathSession+formatter.format(now));           			            			            			
        				deviceManager.connectDevice(empatica);
        				// Depending on your configuration profile, you might be unable to connect to a device.
        				// This should happen only if you try to connect when allowed == false.        				
        				buttonEmpatica.setText("Stop");                		                	
        			} catch (ConnectionNotAllowedException e) {
        				Toast.makeText(getApplicationContext(), "Sorry, can't connect to this device", Toast.LENGTH_SHORT).show();
        			}   
            	}
            	else
            	{            		            		
            		closeEmpatica();            		
            	}	            	
            	         	
                
            }
            });
		
		// Create a new EmpaDeviceManager. MainActivity is both the data and status delegate.
		
				empatica_state = EMPATICA_SCANNING;
		
		
		recording = false;
		chrono = (Chronometer) findViewById( R.id.chrono_activities  );
		timeInit = 0;
		timeEnd  = 0;
		currentActivity = -1;

	    
		
		
	    listActivities = (ListView)findViewById(android.R.id.list);
	    
	    listActivities.setOnItemClickListener( new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Toast.makeText( view.getContext(), "Comenzando", Toast.LENGTH_SHORT ).show();
		    	record();
				currentActivity = (short) position;
				if( connectionSensor57 != null )
				{
					   connectionSensor57.setHarActivity( currentActivity );
					   empaticaLog.UpdateLabel( (int)currentActivity );
				}
				if( connectionSensor60 != null )
				{
					connectionSensor60.setHarActivity( currentActivity );
					empaticaLog.UpdateLabel( (int)currentActivity );
				}
			}
	    	
		}  );
		
		bluetooth = BluetoothAdapter.getDefaultAdapter();
        BluetoothDevice deviceSensor57 = bluetooth.getRemoteDevice("00:07:80:7E:F4:57");     
        BluetoothDevice deviceSensor60 = bluetooth.getRemoteDevice("00:07:80:7F:06:60");
        connectionSensor57 = new DaphneBleConnection( this.getApplicationContext(), deviceSensor57 );
        connectionSensor60 = new DaphneBleConnection( this.getApplicationContext(), deviceSensor60 );
        connectionSensor57.setDirectory( pathSession );
        connectionSensor60.setDirectory( pathSession );
        connectionSensor57.setHarActivity( currentActivity );
        connectionSensor60.setHarActivity( currentActivity );
        buttonConnect = (Button) findViewById( R.id.buttonConnect );
        buttonSession = (Button) findViewById( R.id.buttonNewSession );       
        
        buttonExit = (Button) findViewById( R.id.buttonExit );
        buttonExit.setOnClickListener( new View.OnClickListener() 
        {
        	public void onClick( View v )
        	{
        		((DaphneBleConnection) connectionSensor57).disconnect();
        		((DaphneBleConnection) connectionSensor60).disconnect();
        		((DaphneBleConnection) connectionSensor57).clear();
        		((DaphneBleConnection) connectionSensor60).clear();
        		finish();
        	}
        });
        
        buttonSession.setOnClickListener( new View.OnClickListener()
        {
        	 public void onClick( View v )
        	 {
        		 ((DaphneBleConnection) connectionSensor57).setUnixTimestamp();
        		 ((DaphneBleConnection) connectionSensor60).setUnixTimestamp();
        	 }
         });
         
         buttonConnect.setOnClickListener(new View.OnClickListener() {
             public void onClick(View v) {
             	if(connected)
             	{                     
             		if( connectionSensor57.isConnected() )
             		{
             			connectionSensor57.disconnect();
             			numSensors--;
             			((DaphneBleConnection) connectionSensor57).enableAccelerometer(false);
                        ((DaphneBleConnection) connectionSensor57).enableGyroscope(false);
                        ((DaphneBleConnection) connectionSensor57).clear();	
             		}
             	
             		if( connectionSensor60.isConnected() )
             		{
             			connectionSensor60.disconnect();
             			
             			numSensors--;
             			((DaphneBleConnection) connectionSensor60).enableAccelerometer(false);
                        ((DaphneBleConnection) connectionSensor60).enableGyroscope(false);
                        ((DaphneBleConnection) connectionSensor60).clear();	
             		}
             		
                	if( !connectionSensor57.isConnected() && !connectionSensor60.isConnected() )
             		{
             			connected = false;
             		}
             		buttonConnect.setText("Connect");
             	}
             	else
             	{
             		if( connectionSensor57 != null )
             		{
             			 if( this != null && connectionSensor57.isConnected() )
             		    	{
             		    		numSensors++;
             		    		Toast.makeText( getApplicationContext(), "Connected Sensors: " + String.valueOf( numSensors ) ,Toast.LENGTH_SHORT).show();
             		    		Toast.makeText( getApplicationContext(), "Connected with Sensor #57", Toast.LENGTH_SHORT).show();
            		    	}
             			 
             			((DaphneBleConnection) connectionSensor57).setUnixTimestamp();
                 		((DaphneBleConnection) connectionSensor57).enableAccelerometer(true);
                 		((DaphneBleConnection) connectionSensor57).enableGyroscope(true);
                 		((DaphneBleConnection) connectionSensor57).getStartTimestamp();
                 		
                 		connectionSensor57.addCallback( callback );
                 		
                 		if( connectionSensor57.isConnected() && connectionSensor60.isConnected() )
                 		{
                 			connected = true;
                 			buttonConnect.setText("Disconnect");
                 		}
             		}
             		
             		
             		
             		if( connectionSensor60 != null )
             		{
                 			 if( this != null && connectionSensor60.isConnected() )
                 		    	{
                 		    		numSensors++;
                 		    		Toast.makeText( getApplicationContext(), "Connected Sensors: " + String.valueOf( numSensors ) ,Toast.LENGTH_SHORT).show();
                 		    		Toast.makeText( getApplicationContext(), "Connected with Sensor #60", Toast.LENGTH_SHORT).show();
                		    	} 
                 			 
             			((DaphneBleConnection) connectionSensor60).setUnixTimestamp();
                 		((DaphneBleConnection) connectionSensor60).enableAccelerometer(true);
                 		((DaphneBleConnection) connectionSensor60).enableGyroscope(true);
                 		((DaphneBleConnection) connectionSensor60).getStartTimestamp();	
                 		
                 		connectionSensor60.addCallback(callback);
                 		
                 		if( connectionSensor57.isConnected() && connectionSensor60.isConnected() )
                 		{
                 			connected = true;
                 			buttonConnect.setText("Disconnect");
                 		}	
             		}
             	}
             }
         });
         
         if( this != null && !connectionSensor57.isConnected() )
         {
         	connectionSensor57.connect( this.getApplicationContext() );
         }
         
         if (this != null && !connectionSensor60.isConnected())
         {
         	connectionSensor60.connect(this.getApplicationContext());        	
         	
         }
         callback = new BleCallback() {
             @Override
             public void onCharacteristicChanged(final BluetoothDevice device, final BluetoothGattCharacteristic characteristic) {
                 if (this != null) {                	
                     // Update user interface.
                     	runOnUiThread(new Runnable() {
                         @Override
                         public void run() {
                             adapter.notifyDataSetChanged();
                         }
                     });
                 }
             }
             
             @Override
             public void onCharacteristicRead(BluetoothGattCharacteristic characteristic, int status) {
                 if (status == BluetoothGatt.GATT_SUCCESS && this != null) {               	
                     	runOnUiThread(new Runnable() {
                         @Override
                         public void run() {
                             adapter.notifyDataSetChanged();
                         }
                     });
                 }
             }

             @Override
             public void onDescriptorRead(BluetoothGattDescriptor descriptor, int status) {
                 if (status == BluetoothGatt.GATT_SUCCESS && this != null) {
                     	runOnUiThread(new Runnable() {
                         @Override
                         public void run() {
                             adapter.notifyDataSetChanged();
                         }
                     });
                 }
             }
         };

         adapter = new ListAdapter<BluetoothGattCharacteristic>(this, R.layout.characteristic_list_item) {
             @Override
             protected View getView(View view, BluetoothGattCharacteristic item) {
                             	
             	if( item.getUuid().toString().toLowerCase().equals("36c9AAA1-2486-420f-b40c-bddb99af7c29".toLowerCase()) 
             			|| item.getUuid().toString().toLowerCase().equals("36c9ABA1-2486-420f-b40c-bddb99af7c29".toLowerCase()))
             	{
             	 ((TextView) view.findViewById(R.id.text1)).setText(item.getUuid().toString().substring(4, 8));
                	 ((TextView) view.findViewById(R.id.text2)).setText(Known.CHARACTERISTICS.get(item.getUuid()));
                	 ((TextView) view.findViewById(R.id.text3)).setText(Convert.hexString(item.getValue()));
             	}            	
                 return view;
             }
         };
	}
	
	void closeEmpatica()
	{		
		deviceManager.disconnect();
		empaticaLog.CloseLog();		
		Toast.makeText(this.getApplicationContext(), "Empatica disconnected",Toast.LENGTH_SHORT).show();
		empatica_state=EMPATICA_DISCONNECTED;
	}
	
	@Override
	public void didDiscoverDevice(BluetoothDevice device, int rssi, boolean allowed) {
		// Stop scanning. The first allowed device will do.
		if (allowed) {
			deviceManager.stopScanning();
			empatica=device;
			runOnUiThread(new Runnable() {
				@Override
                public void run() {
					buttonEmpatica.setEnabled(true);
					Toast.makeText(getApplicationContext(),"Empatica connected",Toast.LENGTH_SHORT).show(); 
					empatica_state=EMPATICA_CONNECTED;
                }
				});
						
			
			// Connect to the device			         		

		}
	}

	@Override
	public void didRequestEnableBluetooth() {
		// Request the user to enable Bluetooth
		Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
		startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
	}
	
	public void record()
	{
		if( recording == false )
		{
			recording = true;
			chrono.start();
			timeInit = System.currentTimeMillis();
		}
		else
		{
			recording = false;
			timeEnd = System.currentTimeMillis();
			chrono.setBase( SystemClock.elapsedRealtime() );
		}
	}	
	

	@Override
	public void onDestroy()
	{
		super.onDestroy();
		BleManager.getInstance().clear();
		finish();
	}

	@Override
	public void didReceiveAcceleration(int x, int y, int z, double timestamp) {
		// TODO do something with it
		Log.e("Empatica","Accel "+x+" "+y+" "+z);
		empaticaLog.UpdateAcc(timestamp, x, y, z);
	}

	@Override
	public void didReceiveBVP(float bvp, double timestamp) {
		// TODO do something with it
		Log.e("Empatica","BVP "+bvp);
		empaticaLog.UpdateBVP(timestamp, bvp);
	}

	@Override
	public void didReceiveBatteryLevel(float battery, double timestamp) {
		// TODO do something with it
		Log.e("Empatica","Battery "+battery);
		empaticaLog.UpdateBattery(timestamp, battery);
	}

	@Override
	public void didReceiveGSR(float gsr, double timestamp) {
		// TODO do something with it
		Log.e("Empatica","GSR "+gsr);
		empaticaLog.UpdateGSR(timestamp, gsr);
	}

	@Override
	public void didReceiveIBI(float ibi, double timestamp) {
		// TODO do something with it
		Log.e("Empatica","IBI "+ibi);
		empaticaLog.UpdateIBI(timestamp, ibi);
	}

	@Override
	public void didReceiveTemperature(float temp, double timestamp) {
		// TODO do something with it
		
		Log.e("Empatica","Temperature "+temp);
		empaticaLog.UpdateTemp(timestamp, temp);
		if(empatica_state==EMPATICA_CONNECTED)
		{
			empatica_state=EMPATICA_STREAMING;				
			runOnUiThread(new Runnable() {
				@Override
	            public void run() {
																										
						Toast.makeText(getApplicationContext(),"Empatica streaming",Toast.LENGTH_SHORT).show();
												
	            }
				});				
		}					
		
		
	}

	@Override
	public void didUpdateSensorStatus(EmpaSensorStatus arg0, EmpaSensorType arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void didUpdateStatus(EmpaStatus status) {
		// TODO Auto-generated method stub
		// The device manager is ready for use
				if (status == EmpaStatus.READY) {
					// Start scanning
					deviceManager.startScanning();
					runOnUiThread(new Runnable() {
						@Override
		                public void run() {					
							Toast.makeText(getApplicationContext(),"Scanning for Empatica ...", Toast.LENGTH_SHORT).show();      
		                }
						});							
				// The device manager has established a connection
				} else if (status == EmpaStatus.CONNECTED) {
					// Stop streaming after STREAMING_TIME			
				}
		
	}
}








